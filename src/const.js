
const SLACK_ID = process.env.SLACK_ID;
const SLACK_SECRET = process.env.SLACK_SECRET;

const GITLAB_TOKEN = process.env.GITLAB_TOKEN;
const GITLAB_BASE_URL = 'https://gitlab.com/api/v4';

const CODESHIP_USER = process.env.CODESHIP_USER;
const CODESHIP_PW = process.env.CODESHIP_PW;
const CODESHIP_TOKEN = new Buffer(`${CODESHIP_USER}:${CODESHIP_PW}`).toString('base64');
const CODESHIP_ORG_NAME = process.env.CODESHIP_ORG_NAME;
const CODESHIP_BASE_URL = 'http://api.codeship.com/v2';

const BASIC_TEMPLATE = {
  type: 'basic',
  team_ids: [208918],
  setup_commands: [],
  test_pipelines: [],
  deployment_pipelines: [],
  notification_rules: [],
  environment_variables: [],
};

const PRO_TEMPLATE = {
  type: 'pro',
  team_ids: [208918],
  notification_rules: [],
};

export {
  SLACK_ID,
  SLACK_SECRET,
  GITLAB_TOKEN,
  GITLAB_BASE_URL,
  CODESHIP_BASE_URL,
  CODESHIP_TOKEN,
  CODESHIP_ORG_NAME,
  BASIC_TEMPLATE,
  PRO_TEMPLATE
};
